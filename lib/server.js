#!/usr/bin/node

const express = require("express");
const { insertCommand, getLastCommand } = require("./dbUtils");
const app = express();
const port = 5555;
const commandServer = require("./parse-token-stream-for-commands");

const commands = [];

commandServer(
  process.stdin,
  command => {
    console.log("got a command", command);
    insertCommand(new Date().toString(), command.join(" "));
  },
  {
    delim: "eliza"
  }
);

app.get("/", (req, res) => {
  getLastCommand().then(data => {
    console.log("last command in db", data);
    res.send(true);
  });
});

app.listen(port, () => {
  console.log(`Eliza listening on ${port}`);
});
