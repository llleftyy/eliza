const openSlackVariants = ["eliza open slack", "eliza opened slack"];

function handleCommand(command) {
  console.log("Received command:", command);
  if (openSlackVariants.includes(command.join(" "))) {
    cp.exec(
      "printenv; slack",
      { shell: "/bin/bash" },
      (err, stdout, stderr) => {
        console.log(stdout);
        if (err) console.error(stderr);
        console.log("command to open slack exited");
      }
    );
  } else {
  }
}
