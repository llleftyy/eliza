#!/usr/bin/node

const GetCommandsFromTokenStream = require("./parse-token-stream-for-commands.js");

GetCommandsFromTokenStream(
  process.stdin,
  command => console.log("This is the handled command", command),
  { delim: "eliza" }
);
