const readline = require("readline");

let capturingTokensTimer = null;
let isCapturingTokens = false;
const startCapturingTokens = () => {
  isCapturingTokens = true;
  capturingTokensTimer = setTimeout(stopCapturingTokens, 3000);
};
const stopCapturingTokens = () => {
  isCapturingTokens = false;
  clearTimeout(capturingTokensTimer);
  handleCurrentTokens();
};
const extendCapturingTokens = () => {
  clearTimeout(capturingTokensTimer);
  startCapturingTokens();
};

let currentTokens = [];
/* TODO this is a major temporary stub hack */
let commandHandler = null;
const handleCurrentTokens = () => {
  console.log("handling current tokens:", currentTokens);
  if (currentTokens[0] === "eliza" && currentTokens.length > 1) {
    /* process the token as a command */
    /* TODO shouldn't be using the join here, just pass tokens as found */
    commandHandler(currentTokens.join(" "));
  }

  /* "flush" the already processed tokens */
  currentTokens = [];
};

const handleToken = token => {
  if (token === "eliza") {
    stopCapturingTokens();
    startCapturingTokens();
  }
  if (isCapturingTokens && token.length > 0) {
    currentTokens.push(token);
  }
};

/* Reads line-by-line from source, treating each line as a token, grouping
 * tokens into commands according to the command delimeter, saves those commands
 * to an array, and passes the command to sink (which is a callback). */
const GetCommandsFromTokenStream = (source, sink, config) => {
  commandHandler = sink;
  const rl = readline.createInterface({
    input: source,
    output: null
  });

  rl.on("line", token => {
    console.debug(`Found a token on process.stdin: ${token}`);
    handleToken(token);
  });
};

module.exports = GetCommandsFromTokenStream;
