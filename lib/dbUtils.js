const sqlite = require("sqlite3");
const db = new sqlite.Database(":memory:");

/* Create the commands table */
db.run(
  "CREATE TABLE commands (timestamp string, command string, PRIMARY KEY (timestamp, command))"
);

const logError = err => {
  if (err) console.error(err);
};

const insertCommand = (timestamp, command) => {
  db.run(
    "INSERT INTO commands ('timestamp', 'command') VALUES (?, ?)",
    [timestamp, command],
    logError
  );
};

const getLastCommand = () => {
  return new Promise((resolve, reject) => {
    db.get(
      "SELECT * FROM commands ORDER BY timestamp DESC LIMIT 1",
      [],
      (error, row) => {
        console.log("Values returned by db.get");
        console.log(error);
        console.log(row);
        console.log("--------");
        if (error) {
          reject(error);
        } else {
          resolve(row);
        }
      }
    );
  });
};

module.exports = { insertCommand, getLastCommand };
